import React, { useState } from 'react'

const Login = (props) => {

const { changeState, bought, username, password } = props
   
    const [usernameS, setUsernameS] = useState('')
    const [passwordS, setPasswordS] = useState('')

    const handleChangeUser = (e) => {
        setUsernameS(e.target.value)

    }
    
    const handleChangePassword = (e) => {
        setPasswordS(e.target.value)
    }

    const handleGuess = (e) => {
        if(usernameS === username && passwordS === password) {
            bought()
        }
        else {
            setUsernameS('') 
            setPasswordS('')  
            alert('Wrong username or password')
        }   
    }

    const handleSubmit = (e) => {
        return usernameS && passwordS === '' ? null : handleGuess(e)   
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className='login_box'>
                <p>Do you wanna buy it?</p>
                <div className="inputs">
                    <input className="input" value={usernameS} onChange={handleChangeUser} placeholder="Username:"/>
                    <input className="input" value={passwordS} onChange={handleChangePassword} placeholder="Password:"/>
                </div>

                <div className="buttons">
                    <button className="button" id="confirm" >Confirm</button>
                    <p className="button" id="cancel" onClick={() => changeState()}>Cancel</p>
                </div>
            </div>
        </form>
    )
  }

export default Login