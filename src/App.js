import  React, { useState } from 'react';
import './App.css';
import ProductsList from './products_list';
import Login from './login.js'

function App() {

  const [buyBox, setBuyBox] = useState(false)
  const [displayLogin, setDisplayLogin] = useState(false)
  const [displayBought, setDisplayBought] = useState(false)
  const username = 'nutellaFan'
  const password = '1234'

  const products = [
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    {
      product: 'Nutella',
      price: 10,
      image: 'https://images.unsplash.com/photo-1519420573924-65fcd45245f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    },
    
  ]

  const clickBuy = () => {
    setDisplayLogin(true)
    setBuyBox(true)
}
const changeState = () => {
    setDisplayLogin(false)
    setBuyBox(false)
}
const bought = () => {
  setDisplayLogin(false)
  setDisplayBought(true) 
  setTimeout(() => {setDisplayBought(false)}, 2000)
  setTimeout(() => {setBuyBox(false)}, 2000)
}

  return (
    <div className="App">
      <div className={buyBox ? 'greyout' : null}>
      <header className="App-header">
        <img id="logo" src="https://slashmobility.com/wp-content/uploads/2019/02/img_logo_slashmobility.png"/>
      </header>
      <main>
        <div className="product_grid">
          <ProductsList product={products} displayLogin={displayLogin} clickBuy={clickBuy}/>
        </div>
          
      </main>
      <footer className="footer">
        <p>© Slashmobility 2018</p>
      </footer>
      </div>
      {displayLogin 
                    ? 
                    <div className="login_box_popup">
                        <div>
                            <Login changeState={changeState} displayBought={displayBought} bought={bought}
                                    username={username} password={password}/>
                        </div>
                    </div>
                : null }
      {displayBought
                    ? 
                    <div>
                      <div className="bought_box_popup">
                        <div>
                          <p>{username} bought {products[0].product} <br/> for €{products[0].price}.</p>
                        </div>
                      </div>
                    </div>
                : null }
    </div>
  );
}

export default App;
