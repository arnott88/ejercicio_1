import React from 'react'
import SingleProduct from './single_product.js'


const ProductsList = (props) => {

    const {product, displayLogin, clickBuy } = props


    return (
      <div className='wrapper'>
            {
              product.map((ele, i )=>{
                return <SingleProduct  key ={i} singleItem= {ele} displayLogin={displayLogin} clickBuy={clickBuy}/>                           
                         })
            }
          </div>
    )
  }

export default ProductsList