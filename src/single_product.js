import React from 'react'


const SingleItem = (props) => {
    const { singleItem, displayLogin, clickBuy } = props;

    
return( 
    <div className={displayLogin ? 'greyout' : null} >

        <div className="products_box">
              <img className="image" src = {singleItem.image}></img>
              <h1 id="name" className ="name"> {singleItem.product}</h1> 
              <h2 id="price" className ="price"> ${singleItem.price}</h2>   
              <p className="buy" onClick={() => clickBuy()}>Buy</p>         
            </div>
            
    </div>

    )
}

export default SingleItem