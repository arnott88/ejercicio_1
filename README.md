!!Important!!

Test username: nutellaFan <br />
Test password: 1234

Any other username or password will result in an <br /> 
alert notifying wrong username or password.

I created the page to the best of my <br />
understanding from the exersise description.

If you click on buy you will be prompted to <br />
input a password and username, if entered correctly <br />
a message will be displayed notifying you of your purchase.

Clone repository to test.



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
